var gameState = {
	wait : -1,		//before the game begins.
	guess : 1, 		//players are shown the picture and guess the article title.
	vote : 2,		//players are shown the other players guesses and the actual title. vote for their favorite.
	scoring : 3,	//the actual title is revealed.  everyone who voted for the actual title gets a point.
					//everyone whose guess was voted for gets a point.
	results : 4,	//people's current standings are shown briefly.
}


class Game{
    constructor() {
        this.waitTime =     60;  //timers
        this.guessTime =    45;  //timers
        this.voteTime =     40;  //timers
        this.scoringTime =  10;  //timers
        this.resultsTime =  10;  //timers //potentially should be shorter to allow fetch and parse time in real world scenario.


		this.code = ''; 				//the code to identify the game room.  Players can enter the game by entering the game code.
		this.public = true;				//whether or not the code should be broadcast.
        this.state = gameState.wait; 	//a gamestate enum;
        this.players = [] 				//an array of the players in the game.
		this.guesses = []; 				//an array of guesses
        this.votes = [];				//an array of strings referencing guess.guesses.
		
		this.currentGameState = {};

        

        this.timer = this.waitTime; 		//a timer for the remaining time in the phase of the game.
        
		this.imgUrl = '';  	//a string of the current wikihow img.
		this.articleUrl = ''; // a string of the current wikihow article.
        this.title = 'no title';		//the correct answer.

        this.lameCount = 0;//players can vote that the round is lame.
                            //if enough players vote that it is lame, then skip the round.
        this.dead = false;

        let hello = this.players[0];
    }
    sendMessage(contents){
		this.emitAll('message',new Message('server',contents));
	}
	/**
     * adds a player to the game
     * @param {Player} player
     */
    addPlayer(player) {
        this.players.push(player);
    }
    /**
     * adds a guess to the game.
     * @param {Guess} guess
     */
    addGuess(guess) {
        this.guesses.push(guess);
    }
    /**
     * adds a vote to the room
     * @param {Vote} vote
     */
    addVote(vote) {
        this.votes.push(vote);
    }
	/**
	 * called once every tick of the server.
	 */
	update(){
		if(this.players.length === 0){
			this.dead = true;
		}
        this.timer--;
		
        if (this.timer <= 0) {
            this.moveToNextPhase();
		}
	}
    /**
     * calls a call back once for each player in the game using the player as an argument.
     * @param {Function} callBack the function that is called for each player.  the player is provided as an argument
     */
	forEach(callBack){
		this.players.forEach((x) => callBack(x));
	}
	
	/**
	 * emits a message to all players in this game.
	 * @param {string} message the message tag in the emit
	 * @param {any} content    the content to be sent along with the message tag.
	 */
	emitAll(message,content){
		this.players.forEach((x)=>{
			x.socket.emit(message, content);
        });
	}
	
	getTitleObject(){
		return {
			title: this.title,
			article: this.articleUrl
		};
	}
	
	/**
	 * returns a message that allows new players to at least see what is currently going on.
	 */
	getCurrentGameStateMessage(){
		let stateMessage = '';
		switch(this.state){
			case gameState.wait:
                stateMessage = 'stateWait'; //TODO test that this is the correct value for this?
                break;
            case gameState.guess:
                stateMessage = 'stateGuess';
				break;
            case gameState.vote:
                stateMessage = 'stateVote';
				break;
            case gameState.scoring:
                stateMessage = 'stateScoring';
				break;
            case gameState.results:
                stateMessage = 'stateResults';
				break;
		}
		return {message: stateMessage, state: this.currentGameState};
	}
	/**
	 * calls the relevant setupFunction and moves the state to the next one in the cycle
     * wait->[guess->vote->scoring->results->]
	 */
    moveToNextPhase() {
        switch (this.state) {
            case gameState.wait:
                this.setupGuess();
                return;
            case gameState.guess:
                this.setupVote();
                return;
            case gameState.vote:
                this.setupScoring();
                return;
            case gameState.scoring:
                this.setupResults();
                return;
            case gameState.results:
                this.setupGuess();
                return;
        }
    }
	/**
	 * handles the beginning of a new round.
     * also informs the clients that it is entering the guess state.
     * sends the imgUrl to the clients as well.
	 */
    setupGuess() {
        /////////////////////////
        //reset the game state.//
        /////////////////////////
        this.state = gameState.guess; 		//we already have players, go to guess, not wait.
        this.timer = this.guessTime;
        this.guesses = []; 	//clear all the guesses
        this.votes = [];	//clear all the votes.

        this.imgUrl = '';  	//a string of the current wikihow img.
		this.articleUrl = '';
        this.title = 'no title';		//the correct answer. //TODO do not send the correct answer in the final version of the game. (actually it should, just not at the beginning?) If someone is hacking, then they will cheat regardless anyways.

        this.lameCount = 0;//reset the lame count for the new game.

        ////////////////////////////
        // reset the player state. //
        //////////////////////////////
        //reset the players.
        this.forEach((x) => {
            x.newRound(2, 2); /// number of allowed votes and guesses is passed to the player here.
                               // potentially this should be changed so that a variable is passed instead ;(
        });

        
        //////////////////////////////////////////////////
        // get the next picture and answer.             //
        // and then send the state change to the users. //
        //////////////////////////////////////////////////
        request('http://wikihow.com/Special:Randomizer', (error, response, body) => {
            if (error) {
                console.log(error);
            }
			//get article url
			this.articleUrl = response.request.uri.href;
			
			//get the imgUrl
			let slicedWebPage = body.split('"');
			let imgAddresses = slicedWebPage.filter((x) => {
				return x.startsWith('https://www.wikihow.com/images/thumb');
			});
			this.imgUrl = imgAddresses[Math.floor(imgAddresses.length / 2)];
			console.log(`found imgurl: ${this.imgUrl}`);
			//get the title of the page.
			slicedWebPage = body.split('>');
			let title = slicedWebPage.find((x)=>x.startsWith('How to '));
            this.title = title.split('<')[0].toLowerCase().split('- wikihow')[0].split('(with pictures)')[0]; //breaks off the ' - Wikihow ' if it exists.
            
            //send the new imgUrl to the players.
            this.currentGameState = this.imgUrl;
			this.emitAll('timer',this.guessTime); //todo combine the timer message with the state emission
			this.emitAll('stateGuess', this.currentGameState);
        });
        console.log('request was made.');
    }
    /**
     * a function that sets up the vote state
     * takes all the guesses and sends them to players a a string array. includes the actual answer along with them
     * it is up to the clients to scramble the guesses.
     */
    setupVote() {
        this.state = gameState.vote;
        this.timer = this.voteTime;
        //can't send everyone the same info this time.
        this.forEach(player => {
            let guessesToEmit = this.guesses.filter(g => g.playerId !== player.id) //filters out the player's own guess
                .map(g => g.guess); //strips the guess of all unecessary info
            guessesToEmit.push(this.title); //adds the actual answer as an option.
            player.socket.emit('stateVote', guessesToEmit);
        });
		this.currentGameState = this.guesses.slice().push(this.title); 
		this.emitAll('timer',this.voteTime);
    }
    /**
     * a function that setups the scoring state.
     * handles giving the players their points
     * also builds an array of scoredguesses allowing the clients to see which guesses got the most votes.
     */
    setupScoring() {
        this.state = gameState.scoring;
        this.timer = this.scoringTime;
        //build the ScoredGuess array.
        let scoredGuess = this.guesses.map(x => new ScoredGuess(x, false)); 
        scoredGuess.push(new ScoredGuess(this.title, true));
        for (let vote of this.votes) {
            let votedForScoredGuess = scoredGuess.find(x => x.guess === vote.vote);
            if (votedForScoredGuess) {//make sure the votedForScoredGuess actually exists.
                votedForScoredGuess.vote();//increase the number of votes for this guess.
                if (votedForScoredGuess.isActualAnswer) { //if the voted for guess was the right answer, give the voter a point
                    this.scorePlayer(vote.playerId);
                }
                else {  //otherwise give the voted for player a point.
                    this.scorePlayer(votedForScoredGuess.playerId)
                }
            }
            else { //a player managed to vote for a guess that didn't exist?
                console.log(`${vote.playerId} voted for ${vote.vote} which doesn't exist.`);
            }
        }
        //informs the players that it is scoring time and sends them the scoredGuess array.
        this.currentGameState = scoredGuess;
		this.emitAll('timer',this.scoringTime);
        this.emitAll('stateScoring', this.currentGameState); // TODO there should be a standard gameState object. this keeps changing.
		this.emitAll('title',this.getTitleObject());
    }
    /**
     * a helper function for setupScoring
     * gives the player associated with the playerId provided a point.
     * @param {string} playerId the scoring players id.
     */
    scorePlayer(playerId) {
        let player = this.players.find(x => x.id === playerId);
        if (player) {
            player.score++;
        }
        else {
            console.log(`${playerId}: could not score because could not find this id.`);
        }
    }

    /**
     * moves the game to the setup phase and sends the score result data to all of the players.
     */
    setupResults() {
        this.state = gameState.results;
        this.timer = this.resultsTime;
        let results = this.players.map(x => new Result(x.name, x.score));
		this.currentGameState = results;
		this.emitAll('timer',this.resultsTime);
        this.emitAll('stateResults', this.currentGameState);
		this.emitAll('title',this.getTitleObject());
    }
}

class Player{
    /**
     * creates a player
     * @param {SocketIO.Socket} socket
     */
    constructor(socket) {
        this.id = socket.id;

		this.score = 0;         //the players' points
		this.socket = socket;   //the player's socket.
        this.name = 'no name';  //string representing the player
        //can't have a reference to game.
        //that would be circular.

        //TODO
        //add in a LAME flag, so that people can vote to skip the round if the picture turns
        //out to be lame.
        this.lame = false;
		
		this.remainingGuesses = 0;
		this.remainingVotes = 0;
    }
    /**
     * resets the players remaining guesses and votes.
     * @param {number} guesses
     * @param {number} votes
     */
    newRound(guesses,votes) {
        this.lame = false;
        this.remainingGuesses = guesses;
        this.remainingVotes = votes;
    }
}
class Guess{
    /**
     * creates a guess
     * @param {string} guess
     * @param {Player} player
     */
	constructor(guess,player){
		this.guess = guess;
        this.playerId = player.id;
        this.playerName = player.name;
	}
}
class Vote {
    /**
     * creates a vote
     * @param {string} vote represents a guess
     * @param {Player} player
     */
    constructor(vote, player) {
        this.vote = vote;
        this.playerId = player.id;
    }
}
//is a guess with a player name and the number of people that voted for it.
//also has a flag for whether or not it was the real answer.
class ScoredGuess {
    /**
     * creates a scored guess
     * @param {Guess|string} guess
     * @param {boolean} isActualAnswer
     */
    constructor(guess, isActualAnswer) {
        //guess is treated like a string if it is the actual answer XP
        //otherwise it is treated as a guess.
        if (isActualAnswer) {
            this.guess = guess;
            console.log('the actual answer guess'+ guess);
            this.playerName = 'Title';
            this.playerId = '';
            this.isActualAnswer = true;
            this.score = 0;
        }
        else {
            this.guess = guess.guess;
            this.playerName = guess.playerName;
            this.playerId = guess.playerId;
            this.isActualAnswer = isActualAnswer; // a flag for whether or not this is the actual answer.
            this.score = 0;
        }
    }
    //if a player voted for this, its score goes up.
    vote() {
        this.score++;
    }
}
class Result {
    /**
     * for passing the results back to the players.
     * @param {string} name represents the player in a human readable fashion
     * @param {number} score the score that the player has.
     */
    constructor(name, score) {
        this.playerName = name;
        this.score = score;
    }
}
class Message {
	constructor(owner,contents){
		this.owner = owner;
		this.contents = contents
	}
}
////////////////////
//setup the server//
////////////////////
const express = require('express');
const http = require('http');
const path = require('path');
const socketIO = require('socket.io');
const request = require('request');

const app = express();
const server = http.Server(app);
const port = process.env.PORT || 5000;
const io = socketIO(server);

var games = [];

//////////////
//end points//
//////////////
app.set('port',port);
app.use('/',express.static(__dirname + '/client'));


//TODO can probably get rid of this.
//will leave this here as an example to myself for the moment.
app.get('/api/hello', (req, res) => {
  res.send({ secret: 'hello there :)' });
});

app.get('/api/games', (req, res) => {
	let publicGameCodes = games.filter( g=> g.public).map(g => g.code);
	res.send({ games : publicGameCodes });
});

///////////////////
//begin listening//
///////////////////
server.listen(port, () => {
	console.log(`Listening on port ${port}`);
});

///////////////////////
//respond to messages//
//from the players   //
///////////////////////
io.on('connection', (socket) => {
    let thisPlayer = new Player(socket);
    let game = null;
    console.log(`a connection was made from ${socket.id}`);

    socket.on('joinGame', (content) => {
        console.log(`${socket.id} attempting to join game ${content}`);


        //Check if they are in any other game first.
        //if they are, ignore the request.
        if (game) {//if they are already in a game, do nothing.
            return;
        }

        //search for the game and see if it exists.
        game = games.find((x) => x.code === content);
        if (game) {
            //if it does exist, join the game.
            game.players.push(thisPlayer);
			game.sendMessage('Player ' + thisPlayer.name + ' has joined the game');
            //and tell the player that they have joined a game room.
            socket.emit('haveJoinedGame', content);
            console.log(`${socket.id} succesfully joined game ${content}`);
			//TODO prep the player
			// send them the current picture, then send the current game state.
			socket.emit('imgUrl', game.imgUrl);
			let message = game.getCurrentGameStateMessage();
			socket.emit(message.message,message.state);
			socket.emit('timer',game.timer);//inform the player of the current remaining time.
        }
        //if it does not exist do nothing.
    });
	
	socket.on('setName', (content) => {
		//TODO check that the name is valid?
		let oldName = thisPlayer.name;
        thisPlayer.name = content;
        socket.emit('haveSetName', content);
		if(game){
			game.sendMessage(oldName + ' has changed their name to ' + content);
		}
        console.log(`${socket.id} changed name to ${content}`);
	});

	socket.on('message', (content) => {
		console.log('recieved message ' + content);
		if(game){
			game.emitAll('message',new Message(thisPlayer.name,content));
		}
	});
	
    socket.on('guess', (content) => {
        content = content.toLowerCase();
        console.log(`${socket.id} send guess ${content}`);
        if (thisPlayer.remainingGuesses > 0) {
            thisPlayer.remainingGuesses--;
            game && game.guesses.push(new Guess(content, thisPlayer));
            game && console.log(game.guesses);
        }
        //this is seperate, in the case the client does not recieve doneGuessing the previous time.
		if(thisPlayer.remainingGuesses <= 0) {
			socket.emit('doneGuessing');
		}
	});
	
	socket.on('vote', (content) => {
		//adds a vote to the vote list.
		if(thisPlayer.remainingVotes > 0){
			thisPlayer.remainingVotes--;
			game && game.votes.push(new Vote(content,thisPlayer));
		}
		if(thisPlayer.remainingVotes <= 0){
			thisPlayer.doneVoting = true;
			socket.emit('doneVoting');
		}
    });

    socket.on('lame', () => {
        //if the player hasn't previously voted that this round is lame.
        if (!thisPlayer.lame && game) {
            thisPlayer.lame = true;
            game.lameCount++;//increase the lame count.
			game.sendMessage(thisPlayer.name + ' thinks this image is lame');

            //if more than half the players think the game is lame, then go directly to the next round.
            if (game.lameCount > game.players.length / 2) {
				//immediately sets up the next round.
                game.setupGuess();
				game.sendMessage('The image was too lame. Starting a new round');
            }
        }
    });
	
	socket.on('create', (content) => {
		if(game){
			return; // can't create a game if in a game.
		}
		//make sure the code doesn't already exist
		if(games.find((g)=>g.code === content.code) === undefined){
			game = new Game();
			game.code = content.code;
			game.public = content.public;
			
			//add the game to the list of games.
			games.push(game);
			//add the player to the game.
			game.players.push(thisPlayer);
			socket.emit('timer',game.timer);
			//TODO allow adjusting the timers
			//TODO allow setting max player count
		}
	});
	
	socket.on('start', () => {
		if(game){
			if(game.state === gameState.wait){
				game.timer = 0;
				game.sendMessage( thisPlayer.name + ' has started the game!');
			}
		}
	});
	
	socket.on('leave', () => {
		console.log('recieved leave message');
		if (game) {
            let playerIndex = game.players.findIndex(player => player.id === thisPlayer.id);
            if (playerIndex >= 0) {
                game.players.splice(playerIndex, 1);
            }
			game.sendMessage(thisPlayer.name + ' has left the game.');
			game = null;
        }
	});

	//response to a 'drip'
	socket.on('drop', ()=> {
		console.log(`${socket.id}: drop`);
    });

    socket.on('nextPhase', () => {
        if (game) {
            game.timer = 0;
        }
    });

    socket.on('disconnect', () => {
        if (game) {
            let playerIndex = game.players.findIndex(player => player.id === thisPlayer.id);
            if (playerIndex >= 0) {
                game.players.splice(playerIndex, 1);
            }
			game.sendMessage(thisPlayer.name + ' has left the game.');
        }
    });
});

///////////////
// game loop //
///////////////////////////////////////
// this should primarily just        //
// tick down the timers for the      //
// different games that are going    //
// on.  everything else should       //
// occur in response to user actions //
///////////////////////////////////////
setInterval(() => {
	games.forEach((game)=>{
		game.update();
    });

    //filter out the dead games.
    games = games.filter((x) => x.dead === false);
}, 1000);

