﻿import React, { Component } from 'react';

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

import { Home } from './components/pages/Home';
import { Join } from './components/pages/Join';
import { Create } from './components/pages/Create';
import { About } from './components/pages/About';

import { Guess } from './components/stages/Guess';
import { Vote } from './components/stages/Vote';
import { Scoring } from './components/stages/Scoring';
import { Results } from './components/stages/Results';
import { Wait } from './components/stages/Wait';

import { AppMenu } from './components/AppMenu';
import { AppContent } from './components/AppContent';

import {
	subscribeToPing,
	subscribeToServerMessages,
	subscribeToComments,
	subscribeToServerStateMessages,

	sendMessage,
	sendGuess,
	sendLame,
	sendVote,
	sendNextPhase,
	joinGame,
	setName,

	sendCreate,
	sendStart,
	sendLeave,

	getPublicGamesAsync,

	gameState,
} from './api';

import * as strings from './strings'; // perhaps we could import a different strings file depending on the language of the user's  region?

var pages = Object.freeze({
	home: 0,
	join: 2,
	create: 3,
	about: 4,
});


class App extends Component {
	constructor(props) {
		super(props);

		subscribeToPing(this.increaseValue);
		subscribeToServerMessages(this.onSetState);
		subscribeToServerStateMessages(this.onSetState);
		subscribeToComments(this.handleComment);
		
		this.state = {
			page: pages.home,
			state: gameState.none,  //the game's state.
			img: '',    //the img url.
			article: '', // the article url.
			name: strings.defaultName,  //the player's name.
			title: '', //title of the wiki image.
			timer: 0,
			guesses: [],
			scoredGuesses: [],
			results: [],
			games: [],
			messages: [{owner:'client',contents:strings.welcomeMessage}],
			unread: 0,
		};
		
		this.updateTimer();
	}
	
	handleComment = (comment) => {
		let newMessages = this.state.messages;
		newMessages.push(comment);
		if(newMessages.length > 20) newMessages.shift();
		this.setState({message: newMessages, unread: this.state.unread+1});
	}
	
	updateTimer = () => {
		if(this.state.timer > 0){
			this.setState({
				timer: this.state.timer-1,
			});
		}
		setTimeout(this.updateTimer, 1000);
	}
	
	handleGetPublicGames = () => {
		getPublicGamesAsync().then(games=>this.setState({games: games}));
	}
	
	handleJoinGame = (game) => {
		//should check that you aren't already in a game.
		// so you would have to leave a game before you can join a game.
		this.setState({state: gameState.wait, page: pages.join});
		joinGame(game);
	}
	
	handleCreate = (game) => {
		this.setState({state:gameState.waitCreate, page: pages.join});
		sendCreate(game);
	}

    handleSetName = (name) => {
        setName(name);
    }

	handleSendMessage = (message) => {
		sendMessage(message);
	}
	
    handleGuess = (guess) => {
        if (guess && guess.length > 0) {
            sendGuess(guess);
        }
    }

    handleLame = () => {
        sendLame();
    }

	handleStartGame = () => {
		sendStart();
	}
	
    handleVote = (vote) => {
        sendVote(vote);
    }
	
	handleMenuClick = () => {
		this.setState({
			openMenu: !this.state.openMenu,
		});
	}
	
	handleReadMessages = () => {
		this.setState({
			unread: 0,
		});
	}

	/**
	used to allow the api functions to set the state
	**/
    onSetState = (newState) => {
        this.setState(newState);
    }
	
	renderContent() {
		switch(this.state.page){
			case pages.home:
				return <Home onJoin={this.handleGoJoin} onCreate={this.handleGoCreate} />;
			case pages.join:
				return this.renderJoin();
			case pages.create:
				return <Create onCreate={this.handleCreate} />
			case pages.about: 
				return <About/>
			default:
				return <div> what have you done? </div>
		}
	}
	
	renderJoin(){
		switch (this.state.state){
			case gameState.none:
				return <Join 
					onJoin={this.handleJoinGame} 
					onRefresh={this.handleGetPublicGames} 
					onCreate={this.handleGoCreate} 
					games={this.state.games}
				/>
            case gameState.guess:
                return <Guess
                    timer={this.state.timer}
                    imgSrc={this.state.img}
                    onLame={this.handleLame}
                    onGuess={this.handleGuess}
                />;
            case gameState.vote:
                return <Vote
                    timer={this.state.timer}
                    imgSrc={this.state.img}
                    onVote={this.handleVote}
                    guesses={this.state.guesses}
                />;
            case gameState.scoring:
                return <Scoring
                    timer={this.state.timer}
                    imgSrc={this.state.img}
                    scoredGuesses={this.state.scoredGuesses}
					title={this.state.title}
					article={this.state.article}
                />;
            case gameState.results:
                return <Results
                    timer={this.state.timer}
                    imgSrc={this.state.img}
                    results={this.state.results}
					title={this.state.title}
					article={this.state.article}
                />;
			case gameState.waitCreate:
				return <Wait
					timer={this.state.timer}
					imgSrc={this.state.img}
					creator={true}
					onStart={this.handleStartGame}
				/>
            case gameState.wait:
            default:
                return <Wait
                    timer={this.state.timer}
                    imgSrc={this.state.img}
					creator={false}
					onStart={undefined}
                />
        }
	}
	
	handleGoHome = () => {
		this.setState({page : pages.home});
	}
	
	handleGoJoin = () => {
		this.handleGetPublicGames();
		this.setState({page : pages.join});
	}
	
	handleLeave = () => {
		sendLeave();
		this.setState({state: gameState.none, page: pages.home});
	}
	
	handleGoCreate = () => {
		this.setState({ page: pages.create});
	}
	
	handleGoAbout = () => {
		this.setState({ page: pages.about});
	}
	
	renderMenu() {
		return (
		<React.Fragment>
			<div className='card-space'/>
			<div className={'menu-btn clickable ' + (this.state.page === pages.home ? ' menu-btn-active':null)} onClick={this.handleGoHome}>
				<i className='fas fa-home'/> home
			</div>
			<div className={'menu-btn clickable ' + (this.state.page === pages.join ? ' menu-btn-active':null)} onClick={this.handleGoJoin}>
				<i className='fas fa-gamepad'/> {this.state.state !== gameState.none ? 'game' : 'join'}
			</div>
			{ this.state.state !== gameState.none ? 
				(
					<div className='menu-btn clickable ' onClick={this.handleLeave}>
						<i className='fas fa-door-open'/> leave
					</div>
				):
				(
					<React.Fragment>
						
						<div className={'menu-btn clickable ' + (this.state.page === pages.create ? ' menu-btn-active':null)} onClick={this.handleGoCreate}>
							<i className='fas fa-plus'/> create
						</div>
					</React.Fragment>
				)
			}
			<div className={'menu-btn clickable ' + (this.state.page === pages.about ? ' menu-btn-active':null)} onClick={this.handleGoAbout}>
				<i className='fas fa-question'/> about
			</div>
		</React.Fragment>
		);
	}
    
    render() {
		return (
			<div className='app-main'>
				<AppMenu name={this.state.name} onNameChange={this.handleSetName} onSendMessage={this.handleSendMessage} onReadMessages={this.handleReadMessages} messages={this.state.messages} unread={this.state.unread}>
					{this.renderMenu()}
				</AppMenu>
				<AppContent>
					{this.renderContent()}
				</AppContent>
			</div>
		);
    }
}

export default App;
