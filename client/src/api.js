
import openSocket from 'socket.io-client';
const socket = openSocket('');//http://localhost:5000


/**
 * an enum that defines the gameState values.
 */
export var gameState = {
	none: -2,
    wait: -1,	//before the game begins.
	waitCreate: 0,
    guess: 1, 		//players are shown the picture and guess the article title.
    vote: 2,		//players are shown the other players guesses and the actual title. vote for their favorite.
    scoring: 3,	//the actual title is revealed.  everyone who voted for the actual title gets a point.
    //everyone whose guess was voted for gets a point.
    results: 4,	//people's current standings are shown briefly.
}

export function subscribeToPing(callBack) {
    socket.on('drip', () => {
        console.log('drip received');
        socket.emit('drop');
        callBack();
    });
}

/**
 * subscribe to different messages the server may send at any time.
 * @param {Function} setState
 */
export function subscribeToServerMessages(setState) {
    socket.on('doneGuessing', () => {
        //TODO 
        //move to the waiting screen.
        //becuase you are now done guessing.
    });
    socket.on('doneVoting', () => { //TODO combine all 'done' style events to a singular 'done' event interpretted based on which phase the game is in.
        //TODO
        //do whatever needs to be done when done voting.
        //move to the end of voting screen.
    });
    socket.on('haveJoinedGame', (content) => {
        console.log(`we have joined the game ${content}`)
    });
    socket.on('haveSetName', (content) => {
        setState({ name: content });
    });
    socket.on('imgUrl', (content) => {
        setState({ img: content });
    });
    socket.on('title', (content) => {
        setState({ title: content.title, article: content.article });
    });
    socket.on('timer', (content) => {
        setState({ timer: content });
    });
}

/**
 * subscribe to server messages that result in a state change.
 * @param {Function} setState
 */
export function subscribeToServerStateMessages(setState) {
    socket.on('stateGuess', (content) => {
        //needs to also clear all previous data.
        setState({ state: gameState.guess, img: content });
    });
    socket.on('stateVote', (content) => {
        //shuffle the options before placing them in memory
        //fisher-yates shuffle
        let m = content.length, t, i;
        while (m) {
            i = Math.floor(Math.random() * m--);
            t = content[m];
            content[m] = content[i];
            content[i] = t;
        }
        setState({ state: gameState.vote, guesses: content });
    });
    socket.on('stateScoring', (content) => {
        setState({ 
			state: gameState.scoring, 
			scoredGuesses: content.sort((a,b) => b.score - a.score),//sorts the values. 
		});
    });
    socket.on('stateResults', (content) => {
        setState({ 
			state: gameState.results, 
			results: content.sort((a,b) => b.score - a.score),//sorts the values a single time.
		});
    });
}

export function subscribeToComments(callback){
	socket.on('message', (content) => {
		callback(content);
	});
}

export function sendMessage(message) {
    socket.emit('message', message);
}

export function setName(name){
	socket.emit('setName', name);
}

export function sendGuess(guess){
	socket.emit('guess', guess);
}

export function joinGame(code) {
    socket.emit('joinGame', code);
}

export function sendLame() {
    socket.emit('lame');
}

export function sendVote(vote) {
    socket.emit('vote', vote);
}

export function sendCreate(gameDetails) {
	socket.emit('create', gameDetails);
}

export function sendStart(){
	socket.emit('start');
}

export function sendLeave(){
	socket.emit('leave');
}

export function sendNextPhase() { // used for testing to skip the phase .
    socket.emit('nextPhase');
}

export function getPublicGamesAsync() {
	return fetch('/api/games').then((res)=> {
		return res.json();
	})
	.then((data)=>{
		return data.games;
	});
}