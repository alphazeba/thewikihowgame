import React, {Component} from 'react';
import { Timer } from './Timer';
/**
 * props
 *  imgSrc  string url
 * 	timer
 */
export class WikiHowImg extends Component {	
	preventContextMenu = (e) => {
		e.preventDefault();
	}
	
    render() {
        return (
			<div className='card'>
				<img
					onContextMenu={this.preventContextMenu}
					className='card-img-top imgHider fullWidth'
					src={this.props.imgSrc}
					alt='no cheating'
				/>
				<div className='card-body'>
					<div className='card-num'>{this.props.title}</div>
					<span className='card-text'>{this.props.text}</span>
				</div>
				<Timer timer={this.props.timer}/>
			</div>
				
        );
    }
}
