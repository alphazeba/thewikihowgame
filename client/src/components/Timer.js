import React, { Component } from 'react';
import './Timer.css';
//Timer.js
// props
//	 timer

export class Timer extends Component {
	
	
	render () {
		return (
			<div className='timer-container'>
				<div className='timer-clockBody'/>
				<div className='timer-clockHand'/>
				<div className='timer-content'>
					{this.props.timer}
				</div>
			</div>
		);
	}
}