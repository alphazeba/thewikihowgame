import React, { Component, Fragment } from 'react';


export class Menu extends Component {
	
	_scrollbar = undefined;
	
	constructor(props){
		super(props);
		this.state = {
			mouseOn: false,
			scrolling: false,
			atBottom: true,
		}
	}
	
	handleSendMessage = (e) => {
		e.preventDefault();
		let message = this._chatBox.value;
		this._chatBox.value = '';
		this.props.onSendMessage(message);
	}
	
	handleReadMessages = () => {
		this.props.onReadMessages();
	}
	
	handleBeginScroll = () => {
		this.setState({
			scrolling: true,
			atBottom: false,
		});
	}
	
	handleEndScroll = () => {
		this.setState({
			scrolling: false,
			atBottom: (this._scrollbar && this._scrollbar.getValues().top ===1),
		});
	}
	
	handleScrollToBottom = () => {
		this.scrollToBottom();
	}
	
	scrollToBottom = () => {
		this._scrollbar && this._scrollbar.scrollToBottom();
	}
	
	renderMessage(message, key){
		return(
			<React.Fragment key={key}>
				<div className='card'>
					<div className='appMenu-message-body'>
						<div className='appMenu-message-header'><i className='fas fa-user-circle'/> {message.owner}</div>
						<div className='appMenu-message-contents'>{message.contents}</div>
					</div>
				</div>
				<div className='card-space'/>
			</React.Fragment>
		);
	}
	
	render(){
		if(this.state.atBottom){
			this.scrollToBottom();
		}
		let c = 'appMenu-chat ' + (this.props.open ? 'appMenu-chat-active': undefined);
		let seeNewButtonC = 'btn btn-primary appMenu-chatFeed-seeNewButton ' + (this.state.scrolling || (this._scrollbar && this._scrollbar.getValues().top !== 1)? 'appMenu-chatFeed-seeNewButton-active' : undefined);
		// we will need to be removing the ton of card spaces here.  this is a dank fix.
		return(
			<div className={c}>
				
				<div onMouseEnter={this.handleReadMessages} className='appMenu-chatFeed'>
					<Scrollbars 
						style={{height: '100%'}} 
						ref={c=> {this._scrollbar = c}}
						onScrollStart={this.handleBeginScroll}
						onScrollStop={this.handleEndScroll}
					>
						<div className='appMenu-chatFeed-topSpace'/>
						{this.props.messages.map((m,i)=>this.renderMessage(m,i))}
					</Scrollbars>
					<button className={seeNewButtonC} onClick={this.handleScrollToBottom}>
						<i className='fas fa-caret-down'/>&nbsp;see new messages&nbsp;
						<i className='fas fa-caret-down'/>
					</button>
				</div>
				<div className='appMenu-chatInput'>
					<form onSubmit={this.handleSendMessage}>
						<div className='input-group'>
							<input className='form-control' ref={x=>this._chatBox=x} />
							<div className='input-group-append'>
								<button className='btn btn-primary'>
									<i className='fas fa-paper-plane'/>
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		);
	}
}
