import React, { Component, Fragment } from 'react';
import { Chat } from './interface/Chat';
import './AppMenu.css';
import * as strings from '../strings';


export class AppMenu extends Component {
	_chatBox = undefined;

	constructor(props){
		super(props);
		
		this.state = {
			menuOpen: false,
			chatOpen: false,
			nameOpen: false,
		}
	}

	handleAnyMenuClick = () => {
		this.setState({
			menuOpen: false,
		});
	}
	
	handeMenuButtonClick = () => {
		this.setState({
			menuOpen: !this.state.menuOpen,
		});
	}
	
	handleChatButtonClick = () => {
		if(!this.state.chatOpen){
			this.props.onReadMessages();
		}
		this.setState({
			chatOpen: !this.state.chatOpen,
		});
	}

	handleNameClick = () => {
		this.setState({
			nameOpen: !this.state.nameOpen,
		});
	}
	
	handleNameChange = (e) => {
		e.preventDefault();
        let nameInput = document.getElementById('menuNameInput');
		let name = nameInput.value;
		if(name.length > 30){
			name = name.substr(0,30);
		}
        this.props.onNameChange(name);
		nameInput.value = '';
		this.setState({nameOpen: false});
	}
	
	onSendMessage = (message) => {
		this.props.onSendMessage(message);
	}
	
	onReadMessages = () => {
		this.props.onReadMessages();
	}
	
	renderMenu(){
		let c = 'appMenu-menu ' + (this.state.menuOpen ? 'appMenu-menu-active' : undefined);
		return(
			<div onClick={this.handleAnyMenuClick} className={c}>
				{this.props.children}
			</div>
		);
	}

	renderNameChange(){
		let c = 'appMenu-nameChange ' + (this.state.nameOpen ? 'appMenu-nameChange-active': undefined);
		return(
			<div className={c}>
				<div className='container-fluid'>
					<div className='row'>
						<div className='col align-center'>
							not {this.props.name}?
						</div>
					</div>
					<div className='row'>
						<div className='col'>
							<form onSubmit={this.handleNameChange}>
								<div className='form-group'>
									<input className='form-control' id='menuNameInput' placeholder='Enter name'/>
									<button className='btn btn-success'>set name</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		);
	}
	
	renderHeader(){
		let greeting = this.props.name === strings.defaultName ? 
			(<React.Fragment><i className='fas fa-exclamation-triangle'/> Click here to set name</React.Fragment>) :
			(<React.Fragment><i className='fas fa-user-circle'/> Hello, {this.props.name}</React.Fragment>);
			
		return(
			<React.Fragment>
				<div className='appMenu-bar'>
					<div className='appMenu-title'>
						The WikiHow Game
					</div>
					<div 
						onClick={this.handeMenuButtonClick} 
						className='appMenu-menuButton appMenu-button clickable inline'
					>
						<i className='fas fa-bars'/>
					</div>
					<div 
						onClick={this.handleChatButtonClick} 
						className='appMenu-chatButton appMenu-button clickable inline'
					>
						<i className='fas fa-comment fa-flip-horizontal'>
							{this.props.unread > 0 ? <span>{this.props.unread}</span> : <span/>}
						</i>
					</div>
					<div onClick={this.handleNameClick} className='appMenu-name clickable'>
						{greeting}
					</div>
				</div>
				<div className='appMenu-bar-shadow'/>
			</React.Fragment>
		);
	}
	
	renderFooter(){
		return(
			<div className='appMenu-footer'>
				Experimental Footer.
			</div>
		);
	}
	
	render () {
		return (
			<Fragment>
				{this.renderHeader()}
				{this.renderMenu()}
				<Chat
					open={this.state.chatOpen}
					messages={this.props.messages}
					onSendMessage={this.props.onSendMessage}
					onReadMessages={this.props.onReadMessages}
				/>
				{this.renderNameChange()}
				{this.renderFooter()}
			</Fragment>
		);
	}
}
