// this will be the moving housing container for feedback crap.

import React, { Component } from 'react';
import './AppFrame.css';

import { ContentWindow } from './ContentWindow';

/*
	should recieve an x and y value ranging from 0-1;
	maybe it would have a good idea to have something similar to this that is rooted to an item rather than being 'absolute'
*/
export class AppFrame extends Component {	
	
	constructor (props){
		super(props);
		
		this.state = {
			screenWidth: 0,
			screenHeight: 0,
			styleHeight: 0,
		}
	}
	
	componentDidMount() {
		this.handleResize();
		window.addEventListener('resize', this.handleResize);
	}
	
	componentWillUnmount() {
		window.removeEventListener('resize', this.handleResize);
	}
	
	handleResize = () => {
		
		//test
		let w = window.innerWidth;//300;//window.innerWidth;
		
		let h = window.innerHeight;//300;//window.innerHeight;
		let min = Math.min(w,h);
		
		let actualHeight = Math.min(0.1*h,0.08*w);
		
		this.setState({
			screenWidth: min * 0.7,
			screenHeight: min * 0.7,
			titleHeight: actualHeight,
		});
	}

	handleMenuClick = () => {
		this.props.onMenuClick()
	}
	
	handleTestMenuButton = () => {
		alert('test menu button works');
	}

	render () {
		let styleTitle= {
			height: this.state.titleHeight + 'px',
			fontSize: this.state.titleHeight + 'px',
			lineHeight: this.state.titleHeight + 'px',
		};
		
		let styleScreen = {
			width: this.state.screenWidth + 'px',
			height: this.state.screenHeight + 'px',
		};
		
		let styleControls = {
			width: this.state.screenWidth + 'px',
		}
		
		return (
			<div className='appFrame-body'>
				<img 
					className='appFrame-menuButton' 
					onClick={this.handleMenuClick}
					src={powerImg}
				/>
				<div className='appFrame-titleBar' style={styleTitle}>
					<span className='appFrame-title hideOverflow fontFancy'>
                            The WikiHow Game
					</span>
                </div>
				<div className='appFrame-screenWrap'>
					<div className='appFrame-screen' style={styleScreen}>
						<div className='appFrame-contentContainer' >
							{this.props.children}
						</div>
					</div>
				</div>
				<div className='appFrame-controls'>
					<img style={styleControls} src={controlsImg}/>
				</div>
			</div>
		);
	}
}