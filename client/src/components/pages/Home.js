

import React, { Component } from 'react';

//home.js
// props
//	onJoin
// 	onCreate


export class Home extends Component {
	
	handleJoin = () => {
		this.props.onJoin();
	}
	
	handleCreate = () => {
		this.props.onCreate();
	}
	
	render(){
		return (
			<div>
				<div className='card'>
					<div className='card-body'>
						<h3 className='card-title'>Welcome to The WikiHow Game!</h3>
						<p className='card-text'>
							Relax, grab some friends, and get ready for some great pictures and titles!
						</p>
					</div>
				</div>
				<div className='card-space'/>
				<div className='card'>
					<div className='card-body'>
						<h5 className='card-title'>Get Started</h5>
						<p className='card-text'>
							Start by setting your name by clicking the button on the top bar above!
						</p>
						<p className='card-text'>
							Once you're ready to go, get started by creating a game <span className='link' onClick={this.handleCreate}>here</span>.
						</p>
						<p className='card-text'>
							If you're wanting to join an existing game, go <span className='link' onClick={this.handleJoin}>here</span>.
						</p>
					</div>
				</div>
				<div className='card-space'/>
				<div className='card'>
					<div className='card-body'>
						<h5 className='card-title'>Rules</h5>
						<p className='card-text'>
							The wikihow game is split into 4 main phases
						</p>
						<div className='card-text end-space'>
							<div>
								<b>1: Guess</b>
							</div>
							<div>
								&nbsp;&nbsp;&nbsp;&nbsp;Take a good look at the provided picture and give your best effort to name the title of the article. You will be given 1 point for each vote your title is given in the next phase.
							</div>
						</div>
						<div className='card-text end-space'>
							<div>
								<b>2: Vote</b>
							</div>
							<div>
								&nbsp;&nbsp;&nbsp;&nbsp;You will be provided a list of all the other player's votes including the picture's actual title.  You will not be able to see your own guesses on this screen, don't worry.
							</div>
							<div>
								&nbsp;&nbsp;&nbsp;&nbsp;You are given 2 votes. You don't need to spend them both. 
							</div>
							<div>
								&nbsp;&nbsp;&nbsp;&nbsp;If you vote for the actual title, you will recieve a point. If you vote for another player's title, they will earn a point.
							</div>
						</div>
						<div className='card-text end-space'>
							<div>
								<b>3: Scoring</b>
							</div>
							<div>
								&nbsp;&nbsp;&nbsp;&nbsp;On the scoring screen the guesses will have their owners revealed and the number of votes each was given.
							</div>
						</div>
						<div className='card-text end-space'>
							<div>
								<b>4: Results</b>
							</div>
							<div>
								&nbsp;&nbsp;&nbsp;&nbsp;See the scores of the players on the server.
							</div>
						</div>
					</div>
				</div>
				<div className='card-space'/>
			</div>
		);
	}
}