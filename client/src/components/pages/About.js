

import React, { Component } from 'react';

//about.js


export class About extends Component {
	render(){
		return (
			<div>
				<div className='card-space'/>
				<div className='card'>
					<div className='card-body'>
						<h5 className='card-title'>About The WikiHowGame</h5>
						<p className='card-text'>
							The WikiHow Game is fusion of Lemon's amazing <a className='link' href='https://damn.dog/'>damn.dog</a> idea along with the party aspect of a <a className='link' href='https://jackboxgames.com/'>Jack Box Party Game</a>.
						</p>
						<p className='card-text'>
							I highly suggest both of these.
						</p>
						<p className='card-text'>
							The WikiHow game was made as a fun idea to play with my friends. Though there was an ulterior motive. Most of my work is owned by others, so this is a non proprietary hunk of code I can use to prove I can do some of the things I claim.
						</p>
					</div>
				</div>
				<div className='card-space'/>
				<div className='card'>
					<div className='card-body'>
						<h5 className='card-title'>About Me</h5>
						<p className='card-text'>
							I am Aron Hommas.  I am a software Engineer.
						</p>
						<p className='card-text'>
							If you are interested in the skills I may or may not have, you can go to my linkedin page <a className='link' href='https://linkedin.com/in/aronhommas'>here</a>
						</p>
						<p className='card-text'>
							I like to build projects I have no idea how to make.  Lately, I've been over my head in machine vision.
						</p>
						<p className='card-text'>
							I am interested in sending things to space, breaking things, and fixing things.  <sub className='dramaTiny'>Spacex notice me.</sub>
						</p>
					</div>
				</div>
				<div className='card-space'/>
			</div>
		);
	}
}