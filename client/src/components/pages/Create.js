



import React, { Component } from 'react';

//create.js
// onCreate

export class Create extends Component {
	
	_code = undefined;
	_private = undefined;
	
	handleCreate = (e) => {
		e.preventDefault();
		let game = {
			code: this._code.value,
			public: !this._private.checked,
		}
		this.props.onCreate(game);
	}

	
	render(){
		return (
			<div>
				<div className='card'>
					<div className='card-body'>
						<h5 className='card-title'>Create a game</h5>
						<form onSubmit={this.handleCreate}>
							<div className='row'>
								<div className='col'>
									<div className='input-group'>
										<div className='input-group-prepend'>
											<span className='input-group-text'>
												Game name: 
											</span>
										</div>
										<input className='form-control' ref={x=>this._code=x} />
									</div>
								</div>
							</div>
							<div className='row'>
								<div className='col'>
									<div className='form-check'>
										<input type='checkbox' className='form-check-input' ref={x=>this._private=x} />
										<label className='form-check-label'> game is private </label>
									</div>
								</div>
							</div>
							<div className='row'>
								<div className='col'>
									<button className='btn btn-success'>Create</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div className='card-space'/>
			</div>
		);
	}
}