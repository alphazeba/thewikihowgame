

import React, { Component } from 'react';

//join.js
// onRefresh
// games
// onJoin

export class Join extends Component {
	
	handleJoin = (game) => {
		this.props.onJoin(game);
	}
	
	handleRefresh = () => {
		this.props.onRefresh();
	}
	
	handleCreate = () => {
		this.props.onCreate();
	}
	
	renderPublicGames(){
		if(this.props.games.length < 1){
			return (
			<li 
				key={'nogames'}
				className='list-group-item clickable'
			>
				<div className='row'>
					<div className='col-12'>
						looks like there aren't any games.  Go <span className='link' onClick={this.handleCreate}>here</span> to make one.
					</div>
				</div>
			</li>
			)
		}
		
		return this.props.games.map(g => 
		<li
			key={g}
			onClick={()=>this.handleJoin(g)}
			className='list-group-item clickable'
		>
			<div className='row'>
				<div className='col-10'>
					{g}
				</div>
				<div className='col-2'>
					<button className='btn btn-success btn-block'>Join</button>
				</div>
			</div>
		</li>
		);
	}
	
	render(){
		return (
			<div>
				<div className='card'>
					<div className='card-body'>
						<h5 className='card-title'>Join a server by name</h5>
						<form onSubmit={this.handleSubmit}>
							<div className='input-group'>
								<input ref={(input) => this.guessInput = input}  className='form-control' />
							</div>
							<button className='btn btn-success'>Join</button>
						</form>
					</div>
				</div>
				<div className='card-space'/>
				<div className='card'>
					<div className='card-body'>
						<h5 className='card-title'>Join a public game</h5>
						
					</div>
					<ul className='list-group list-group-flush'>
						<li
							className='list-group-item'
						>
							<button className='btn btn-primary' onClick={this.handleRefresh}>Refresh</button>
						</li>
					{this.renderPublicGames()}
					</ul>
				</div>
				<div className='card-space'/>
			</div>
		);
	}
}