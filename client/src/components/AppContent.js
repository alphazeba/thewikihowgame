import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
//AppContent.js
import './AppContent.css';

export class AppContent extends Component {
	render() {
		return (
			<div className='appContent-main'>
				<div ref={x=>this._content=x} className='appContent-scrollWrap'>
					<Scrollbars style={{height: '100%'}}>
						<div className='appContent-header'/>
						<div className='appContent-widthHandler'>
							<div className='container'>
								<div className='row'>
									<div className='col-12'>
										{this.props.children}
									</div>
								</div>
							</div>
						</div>
						<div className='appContent-footer'/>
					</Scrollbars>
				</div>
			</div>
		);
	}
}