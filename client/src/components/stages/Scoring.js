import React, { Component } from 'react';
import { WikiHowImg } from '../WikiHowImg';

/**
	STEP 4

    props
    timer
    totalTime   the original time given
    scoredGuesses
    imgSrc  string url
	title  string article name
	article string article url

 * This will show the votes and how many points they made along with who made the guess
    will display if the vote were the real one as well.
	
	this should continue showing the link to the real article.
 */

export class Scoring extends Component {
	renderScores(){
		return this.props.scoredGuesses.map((g,i) => 
			<React.Fragment key={i}>
				<div className='card col-12'>
					<div className='card-body'>
						<div className='row'>
							<div className='col-12'>
								<div className='stickLeft'>
									{g.score} &nbsp; <i className='fas fa-thumbs-up'/>
								</div>
								<div className='containRight scoring-guess'>
									{g.guess}
									<div className='scoring-title'>
										<i className='fas fa-user-circle'/> &nbsp; {g.playerName}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className='card-space'/>
			</React.Fragment>
			);
	}

    render() {
        return (
			<div>
				<WikiHowImg imgSrc={this.props.imgSrc} timer={this.props.timer} title='3' text="How'd it go?"/>
				
				<div className='card-space'/>
				<div className='card col-12'>
					<div className='card-body'>
						<div className='row'>
							<div className='col-12'>
								The real answer was <a className='link' href={this.props.article} target="_blank">{this.props.title}!</a>
							</div>
						</div>
					</div>
				</div>
				
				<div className='card-space'/>
				{this.renderScores()}
			</div>
        );
    }
}