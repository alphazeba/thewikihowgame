import React, { Component } from 'react';

import { WikiHowImg } from '../WikiHowImg';

/**
	STEP 2

    props
    timer
    guesses
    imgSrc  string url
    onVote
    

 * will prominently display the image to guess about.
 * will need to have the LAME button.
 * will need to have the guess dialogue.
    guess bar is auto selected.
    enter sends the guess.
    on guess sent, the guess bar is emptied.
    will need to display the timer
    needs the imgUrl

    will also need whatever other information the waiting screen requires.
 */

export class Vote extends Component {
	
	constructor(props){
		super(props);
		
		this.state = {
			votes : [],
		}
	}

	handleVote = (vote,index) => {
		if(this.state.votes.length >= 2){
			return;
		}
		this.props.onVote(vote);
		this.setState({
			votes: [...this.state.votes, index],
		});
	}

	renderVoteButtons(){
		return this.props.guesses && this.props.guesses.map && this.props.guesses.map((g,i)=>{
			let ani = '';
			let c = 'fas fa-thumbs-up vote';
			switch(this.state.votes.filter(v=>v===i).length){
				case 1:
					c += ' vote-chosen';
					ani = 'vote-animation-bounce';
					break;
				case 2:
					c += ' vote-veryChosen';
					ani = 'vote-animation-wiggle';
					break;
				default:
					break;
			}
			return(
				<li 
					key={i}
					onClick={()=>this.handleVote(g,i)} 
					className='list-group-item clickable card-clickable'
				>
					<div className='row'>
						<div className='col-12'>
							<div className='containLeft'>
								{g}
							</div>
							<div className='stickRight'>
								<div className={ani}>
									<i className={c}/>
								</div>
							</div>
							&nbsp; {/* this provides the lineheight for an otherwise aparently empty column */}
						</div>
					</div>
				</li>
			);
		});
	}

    render() {
        return (
			<div>
				<WikiHowImg imgSrc={this.props.imgSrc} timer={this.props.timer} title='2' text='Choose your favorite'/>
				<div className='card-space'/>
				<div className='col-12'>
					{2 - this.state.votes.length} remaining votes
				</div>
				<div className='card col-12'>
					<ul className='list-group list-group-flush'>
					{this.renderVoteButtons()}
					</ul>
				</div>
			</div>
        );
    }
}