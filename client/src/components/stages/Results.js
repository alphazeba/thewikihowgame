import React, { Component } from 'react';

import { WikiHowImg } from '../WikiHowImg';
/**
	STEP 3

props	
    timer
    results
    imgSrc 
	title  string article name
	article string article url

 * This will show the votes and how many points they made along with who made the guess
    will display if the vote were the real one as well.
	
	will also show a link to the article
 */

export class Results extends Component {
	renderResults(){
		return this.props.results.map((r,i)=> 
			<React.Fragment key={i}>
				<div className='card col-12'>
					<div className='card-body'>
						<div className='row'>
							<div className='col-12'>
								<div className='stickLeft'>
								{r.playerName}
								</div>
								<div className='stickRight'>
								{r.score} &nbsp; <i className='fas fa-thumbs-up'/>
								</div>
								&nbsp;
							</div>
						</div>
					</div>
				</div>
				<div className='card-space'/>
			</React.Fragment>
		);
	}

    render() {
        return (
			<div>
				<WikiHowImg imgSrc={this.props.imgSrc} timer={this.props.timer} title='4' text='How everyone stacks up'/>
				
				<div className='card-space'/>
				<div className='card col-12'>
					<div className='card-body'>
						<div className='row'>
							<div className='col-12'>
								The real answer was <a className='link' href={this.props.article} target="_blank">{this.props.title}!</a>
							</div>
						</div>
					</div>
				</div>
				
				<div className='card-space'/>
				{this.renderResults()}
			</div>
        );
    }
}