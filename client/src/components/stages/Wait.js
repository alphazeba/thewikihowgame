import React, { Component } from 'react';
import { WikiHowImg } from '../WikiHowImg';

/**
 * props
    timer
	creator
	onStart
 */
export class Wait extends Component {
	
	img = 'https://www.wikihow.com/images/thumb/8/8e/Learn-to-Wait-for-What-You-Want-Step-6-Version-2.jpg/aid67738-v4-728px-Learn-to-Wait-for-What-You-Want-Step-6-Version-2.jpg';
	
	handleStart = () => {
		this.props.onStart();
	}
	
	renderStartButton = () => {
		if(this.props.creator){
			return (
				<div className='card col-12'>
						<h5 className='card-title'>
							Start the game when you are ready
						</h5>
						<p className='card-text'>
							Hey, you created a game!
						</p>
						<p className='card-text'>
							When you want the game to start, press the button below to begin the game!  After a period of time, the game will start automatically.
						</p>
						<div className='card-text'>
							<button className='btn btn-success' onClick={this.handleStart}>
								Start
							</button>
						</div>
				</div>
			);
		}
	}
	
    render() {
        return (
			<div>
				<WikiHowImg imgSrc={this.img} timer={this.props.timer} title='0' text='Waiting for the game to be started'/>
				<div className='card-space'/>
				{this.renderStartButton()}
			</div>
        );
    }
}