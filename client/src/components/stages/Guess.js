
import React, { Component } from 'react';
import { WikiHowImg } from '../WikiHowImg';

/**
	STEP 1

    props
    imgSrc  string url
    onLame  fn
    onGuess fn(string)
    

 * will prominently display the image to guess about.
 * will need to have the guess dialogue.
    guess bar is auto selected.
    enter sends the guess.
    on guess sent, the guess bar is emptied.
    will need to display the timer
    needs the imgUrl

    will also need whatever other information the waiting screen requires.
 */

export class Guess extends Component {

    guessInput = null;
	
	constructor(props){
		super(props);
		
		this.state = {
			guesses: [],
		}
	}

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.guessInput) {
			let guess = 'How to ' +this.guessInput.value;
            this.props.onGuess(guess);
            this.guessInput.value = '';
			this.setState({guesses : [...this.state.guesses,guess]});
        }
    }
	
	handleLame = (e) => {
		e.preventDefault();
		this.props.onLame();
	}
	
	renderPreviousGuesses(){
		return this.state.guesses.map((g,i)=> <li key={i} className='list-group-item'>{g}</li>);
	}
	renderGuessForm(){
		if(this.state.guesses.length < 2){
			return (
				<li className='list-group-item'>
					<form onSubmit={this.handleSubmit}>
						<div className='input-group'>
							<div className='input-group-prepend'>
								<span className='input-group-text'>
									How to:
								</span>
							</div>
							<input ref={(input) => this.guessInput = input}  className='form-control' />
						</div>
						<button className='btn btn-success'>Guess</button>
					</form>
				</li>
			);
		}
	}

    render() {		
        return (
		<div>
			<WikiHowImg imgSrc={this.props.imgSrc} timer={this.props.timer} title='1' text='give your best guess as to what this is'/>
			<div className='card-space'/>
			<div className='col-12'>
				{2 - this.state.guesses.length} remaining guesses
			</div>
			<div className='card col-12'>
				<ul className='list-group list-group-flush'>
					{this.renderPreviousGuesses()}
					{this.renderGuessForm()}
				</ul>
			</div>
			<button onClick={this.handleLame} className='btn btn-danger'>Lame</button>
		</div>

        );
    }
}